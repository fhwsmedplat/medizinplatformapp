package com.tuv001.medizinplatform;
/**
 * Created by JTJ
 */
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Symtoms_Anfrage extends AppCompatActivity {

    private Symtom symtomHandler;
    private SymtomSelectedAdapter adapter;
    private ArrayList<Symtom> symtomList;
    private ListView listView;
    private Button button;
    protected ArrayList<String> symtomselected; //entered values by the user
    private String disease; //to save the disease


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symtoms__anfrage);
        listView = (ListView) findViewById(R.id.listview);
        button = (Button) findViewById(R.id.selectedbtn);
        setWidget();
    }

    private void setWidget()
    {
        //Recovering from SharedPreferences the username
        SharedPreferences prefs = getSharedPreferences("Mypreferences", Context.MODE_PRIVATE);
        final String UsernameShared=prefs.getString("username","forexampleusername");//username


        //Symtoms multi language
        String headache=getResources().getString(R.string.headache);
        String fever=getResources().getString(R.string.fever);
        String diarrhea=getResources().getString(R.string.diarrhea);
        String stomach=getResources().getString(R.string.stomach);
        String swelling=getResources().getString(R.string.Swelling);
        String sore=getResources().getString(R.string.sore);
        String nausea=getResources().getString(R.string.nausea);
        String sniff=getResources().getString(R.string.sniff);
        String joint=getResources().getString(R.string.joint);
        String earache=getResources().getString(R.string.earache);
        String photosensitivity=getResources().getString(R.string.photosensitivity);
        String lossofappetite=getResources().getString(R.string.lossofappetite);
        String swelingofeyes=getResources().getString(R.string.swelingoftheeyes);
        String palpitations=getResources().getString(R.string.palpitations);
        String chills=getResources().getString(R.string.Chills);

        final String[] symtomsmultilanguage={headache,fever,diarrhea,stomach,swelling,sore,nausea,sniff
        ,joint,earache,photosensitivity,lossofappetite,swelingofeyes,palpitations,chills};//multi language list of symptoms

        //Adding data to ArrayList
        symtomList = new ArrayList<Symtom>();

        //Loop multi language
        for(int i=0;i<symtomsmultilanguage.length;i++)
        {
           symtomList.add(new Symtom(symtomsmultilanguage[i],false));
        }


        //Setup Adapter for Listview
        adapter = new SymtomSelectedAdapter(Symtoms_Anfrage.this, symtomList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int position, long l) {
                //Symtom Selected from list
                adapter.setCheckBox(position);

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                symtomselected=new ArrayList<String>();



                //to Save the Symptoms selected by the user.

                for (Symtom hold : adapter.getAllData()) {
                    if (hold.isCheckbox())
                    {
                        symtomselected.add(hold.getSymtom());
                    }
                }

                 //Method gettranslation(symtomselected)
                 //To save the symptoms selected by the user in German,
                // because the algorithm(in the Server)need the symptoms in German.

                 symtomselected=gettranslation(symtomselected); //Translated Symptoms from Spanish/English into German


                /*
                // Missing algorithm implementation
                // input:sytomselected  and output:disease
                // APP input->WebService-Server->Server-Algorithm->output APP
                // Method that sends the data=symtomselected to the server
                // and receives from the server the disease (String disease)
                // for example, disease=examplevoid(symtomselected);
                // Missing Query DB to save the value the disease from this user
                 */


                // GUI Messages and Intent
                if(symtomselected.size()>5) //Maximum five symptoms for the algorithm
                {
                    Toast.makeText(Symtoms_Anfrage.this,getResources().getString(R.string.selectuptofivesymptoms), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //message
                    String messagethanks=getResources().getString(R.string.messagethankscooperation);
                    String messagegotomedicaldiagnosis=getResources().getString(R.string.gotomedicaldiagnostic);
                    Toast.makeText(Symtoms_Anfrage.this,messagethanks+" "+UsernameShared+"\n"+messagegotomedicaldiagnosis, Toast.LENGTH_LONG).show();


                    //Intent
                    Intent intent=new Intent(Symtoms_Anfrage.this,PatientMain.class);//Go to PatientMain
                    startActivity(intent);//
                }


            }

        });
    }

    //Method that translates symtoms from Spanish or English into German
    private ArrayList<String> gettranslation(ArrayList<String> symtomsselected)
    {
        ArrayList<String> symtomselectedtranslated=new ArrayList<String>();
        Map<String,String> english=new HashMap<String, String>();
        Map<String,String> spanish=new HashMap<String, String>();
        english.put("Headache","Kopfschmerzen"); spanish.put("Dolor de cabeza","Kopfschmerzen");
        english.put("Fever","Fieber");spanish.put("Fiebre","Fieber");
        english.put("Diarrhea","Durchfall");spanish.put("Diarrea","Durchfall");
        english.put("Stomach pain","Bauchschmerzen");spanish.put("Dolor abdominal","Bauchschmerzen");
        english.put("Swelling on the neck","Schwellung am Hals");spanish.put("Inflamación en el cuello","Schwellung am Hals");
        english.put("Sore throat","Halsschmerzen");spanish.put("Dolor de garganta","Halsschmerzen");
        english.put("Nausea","Übelkeit");spanish.put("Náusea","Übelkeit");
        english.put("Sniff","Schnupfen");spanish.put("Constipado","Schnupfen");
        english.put("Joint","Gelenkschmerzen");spanish.put("Artralgia","Gelenkschmerzen");
        english.put("Earache","Ohrenschmerzen");spanish.put("Dolor de oídos","Ohrenschmerzen");
        english.put("Photosensitivity","Lichtempfindlichkeit");spanish.put("Fotosensibilidad","Lichtempfindlichkeit");
        english.put("Loss of appetite","Appetitlosigkeit");spanish.put("Pérdida de apetito","Appetitlosigkeit");
        english.put("Swelling of the eyes","Schwellungen der Augen");spanish.put("Ojos Hinchados","Schwellungen der Augen");
        english.put("Palpitations","Herzrasen");spanish.put("Palpitaciones","Herzrasen");
        english.put("Chills","Schüttelfrost");spanish.put("Escalofríos","Schüttelfrost");

        //Get the current language
        String language= Locale.getDefault().getLanguage();

        if(language.equals("es"))
        {
            for (String x:symtomselected)
            {
                symtomselectedtranslated.add(spanish.get(x));

            }

        }
        else if(language.equals("en"))
        {
            for(String x:symtomselected)
            {
                symtomselectedtranslated.add(english.get(x));
            }
        }
        else if(language.equals("de"))
        {
            symtomselectedtranslated=symtomsselected;
        }
        return symtomselectedtranslated;
    }
}
