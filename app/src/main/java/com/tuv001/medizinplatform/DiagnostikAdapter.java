package com.tuv001.medizinplatform;

/**
 * Created by JTJ on 02.05.2017.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

class DiagnostikAdapter extends BaseAdapter {
    private Context activity;
    private ArrayList<Diagnostik> data;
    private static LayoutInflater inflater = null;
    private View vi;
    private DiagnostikAdapter.ViewHolder viewHolder;

    public DiagnostikAdapter(Context context, ArrayList<Diagnostik> diagnostiks) {
        this.activity = context;
        this.data = diagnostiks;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        vi = view;
        //Populate the Listview
        final int pos = position;
        Diagnostik diagnostiks = data.get(pos);
        if(view == null) {
            vi = inflater.inflate(R.layout.listview_diagnostik, null);
            viewHolder = new DiagnostikAdapter.ViewHolder();
            viewHolder.disease = (TextView) vi.findViewById(R.id.disease);
            viewHolder.medicine=(TextView) vi.findViewById(R.id.medicine);
            vi.setTag(viewHolder);

        }else
            viewHolder = (DiagnostikAdapter.ViewHolder) view.getTag();
        viewHolder.disease.setText(diagnostiks.getDisease());
        viewHolder.medicine.setText(diagnostiks.getMedicine());
        return vi;
    }
    public ArrayList<Diagnostik> getAllData(){
        return data;
    }


    public class ViewHolder{
        TextView disease;
        TextView medicine;

    }
}
