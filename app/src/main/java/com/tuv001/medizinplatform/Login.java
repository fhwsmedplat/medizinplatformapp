package com.tuv001.medizinplatform;
/**
 * Created by JTJ-PC
 */
//GUI (JTJ)
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.Locale;

//Web service(Magommed)
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONException;
import org.json.JSONObject;




public class Login extends AppCompatActivity {

    private Button buttonregister; // To redirect from Main activity to Register activity
    private Button buttonlogin; //To redirect from Main activity to Patient's main
    private Button buttonchangelanguage;//To change the language
    private EditText etusername; //EditText UserId
    private EditText etpassword; //EditText PasswordUser
    protected String enteredusername; //To save the username value entered by the user
    protected String enteredpassword; //To save the password value entered by the user
    private Locale locale;
    private Configuration config = new Configuration();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buttonchangelanguage=(Button)findViewById(R.id.buttonchangelanguage);//buttonchangelanguage correspond with ID buttonchangelanguage from activity_login.xml
        buttonregister=(Button)findViewById(R.id.buttonRegister); //buttonregister correspond with ID buttonRegister from activity_login.xml
        buttonlogin=(Button)findViewById(R.id.buttonLogin);//buttonlogin correspond with ID buttonlogin from activity_login.xml
        etusername=(EditText)findViewById(R.id.username); //etusername correspond with ID username from activity_login.xml
        etpassword=(EditText)findViewById(R.id.password); //etpassword correspond with ID password from activity_login.xml


        //Button changelanguage
        //If you click the button, then the showdialog() method starts.
        buttonchangelanguage.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        showDialog();
                    }});

        //Button register
        //If click on the button, then redirect to Register
        buttonregister.setOnClickListener(new View.OnClickListener() //if click on Create Account
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(Login.this,Register.class);
                startActivity(intent); //Start
            }
        });

        //Button Login
        buttonlogin.setOnClickListener(new View.OnClickListener() { //If click on the button Login
            @Override
            public void onClick(View v)
            {
                enteredusername = etusername.getText().toString(); //username value entered by the user
                enteredpassword = etpassword.getText().toString(); ////password value entered by the user

                //Messages if something is wrong

                if(enteredusername.equals("")||enteredpassword.equals(""))
                {
                    Toast.makeText(Login.this,R.string.messagefilllogin,Toast.LENGTH_LONG).show();
                    //Error msg if username or password is equals " "
                }

                //Connection App->Web Service
                //When App connect to the server and the response
                //from the DB is username and password is correct then
                //Redirecting to PatientMain

                //else created by Magommed and JTJ
                else {

                    RequestParams params = new RequestParams();
                    // Put Http parameter username with value of Email Edit View control
                    params.put("username", enteredusername);
                    // Put Http parameter password with value of Password Edit Value control
                    params.put("password", enteredpassword);
                    // Invoke RESTful Web Service with Http parameters
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.get("http://192.168.178.60:8080/MedicineWebservice/login/dologin", params, new AsyncHttpResponseHandler() {


                        @Override
                        public void onSuccess(String response) {
                            try {
                                // JSON Object
                                JSONObject obj = new JSONObject(response);
                                // When the JSON response has status boolean value assigned with true
                                if (obj.getBoolean("status"))
                                {
                                    switchScreen();//JTJ(GUI)
                                } else {
                                    Toast.makeText(Login.this,getResources().getString(R.string.invalidcredential), Toast.LENGTH_LONG).show();//GUI JTJ
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                Toast.makeText(Login.this,getResources().getString(R.string.errorconnection), Toast.LENGTH_LONG).show();//GUI JTJ
                                e.printStackTrace();

                            }


                        }

                    });
                }
            }
        });
    }


    public void switchScreen(){
        String language = Locale.getDefault().getLanguage();
        //Welcome message
        Toast.makeText(Login.this, getResources().getString(R.string.welcomemessage), Toast.LENGTH_LONG).show();
        //Intent from Login to PatientMain
        Intent intent = new Intent(Login.this, PatientMain.class);
        //SharedPreferences
        SharedPreferences prefs = getSharedPreferences("Mypreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        //To Save in SharedPreferences
        editor.putString("username", enteredusername);
        editor.commit();
        startActivity(intent);
    }

    //Method showDialog created by JTJ
    /**
     *Displays a dialog box to choose the new application language
     *When you click on one of the languages, the language of the application is changed
     * And reload the activity to see the changes
     * */
    private void showDialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle(getResources().getString(R.string.changelanguage));
        // get the array languages of string.xml
        String[] types = getResources().getStringArray(R.array.languages);
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch(which){
                    case 0:
                        locale = new Locale("en"); //English (default)
                        config.locale =locale;
                        break;
                    case 1:
                        locale = new Locale("es"); //Spanish
                        config.locale =locale;
                        break;
                    case 2:
                        locale = new Locale("de"); //German
                        config.locale =locale;
                        break;
                }
                getResources().updateConfiguration(config, null);
                Intent refresh = new Intent(Login.this, Login.class);
                startActivity(refresh);
                finish();
            }

        });
        b.show();
    }
}
